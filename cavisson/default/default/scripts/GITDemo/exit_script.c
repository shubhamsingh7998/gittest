/*-----------------------------------------------------------------------------
    Name: exit_script
    Recorded By: Shubham
    Date of recording: 12/07/2021 12:43:40
    Flow details:
    Build details: 4.6.1 (build# 84)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

int exit_script()
{
    return 0;
}
