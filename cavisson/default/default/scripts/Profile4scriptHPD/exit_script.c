/*-----------------------------------------------------------------------------
    Name: exit_script
    Recorded By: Shubham
    Date of recording: 11/30/2021 10:52:54
    Flow details:
    Build details: 4.8.0 (build# 1)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

int exit_script()
{
    return 0;
}
