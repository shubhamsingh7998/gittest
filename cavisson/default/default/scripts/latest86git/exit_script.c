/*-----------------------------------------------------------------------------
    Name: exit_script
    Recorded By: shubham
    Date of recording: 12/13/2021 04:39:17
    Flow details:
    Build details: 4.6.1 (build# 86)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

int exit_script()
{
    return 0;
}
